\version "2.19.31"

%\include "lilyjazz.ily"
%\include "jazzchords.ily"

\paper {
  %#(set-paper-size "a4")
  paper-height = 300\cm
  paper-width = 30\cm
  %indent = 1.9\cm
  %short-indent = 1\cm
}

\score {

    \new Staff 
    {

      \clef treble
      \time 4/4
      \set Timing.baseMoment = #(ly:make-moment 1/4)
      \set Timing.beatStructure = #'(1 1 1 1)

      \relative a {
        
        \tempo 4 = 87
        r4 r16 gis a b c d ees f fis gis a bes
        c8 \acciaccatura {gis8} a16 d, f8 g ~ g4. r8
        r16 \acciaccatura {gis16} <a c>16^> g f <g c>8^> f16 d <f c'>16 \parenthesize d <g c>16 q \appoggiatura {gis16} a8 c16 d
        \acciaccatura {gis,8^>} a8 g16 f g f d c f \acciaccatura {gis,8} a16^> g f g8 f16 f
        r2 \tuplet 6/4 { r8 cis'16 d dis fis } bes c f ees
        \appoggiatura {ees8} d8 a16 c a bes d f \acciaccatura {g16 gis} a8 g16 f d c bes \acciaccatura {gis8} a16
        c \acciaccatura {gis8} a16^> f d g8 r16 f ees aes c ees aes bes c c
        \acciaccatura {gis8} a8 f16( d) g8 r16 c \acciaccatura {gis8} a8 f16 g r d c' c
        \acciaccatura {gis8} a8 f16( d) g f \acciaccatura {dis8} e16 ees-- d des-- c aes-- e ees8-- des16
        \tempo 4 = 90
        c ees bes aes g aes c ees \acciaccatura {f16 fis} g8 ees16 c f8 <bes, bes'> ~
        \tempo 4 = 87
        q2 r16 c'32( aes e16) ees_- des c_- bes aes_-
        g bes c ees_- a, bes d f_- b, c ees g_- d ees g bes_-
        \tempo 4 = 88
        e, f aes c_- g aes d ees^- bes des e aes <ees ees'>8 <des des'>16 <ees ees'>
        c'8. bes32 aes g8 f16 g ~ g \acciaccatura {c,8} d16 c g f c g' f
        c8 c16 d ~ d d ees8 ees16 e8 e16 fis8 fis16 g ~
        \tempo 4 = 89
        g c,8 c16 ~c bes c \acciaccatura {fis8} g16 ~ g c,8 bes16 c8 \acciaccatura {fis8} g
        c,16 bes c \acciaccatura {fis8} g16 ~ g bes, c8 aes'16 c, ees aes c ees aes bes
        %c \acciaccatura {gis8} a16 f d g8 \repeat tremolo 4 <c, c'>32 ~ \repeat tremolo 8 <c c'>32 ~ \repeat tremolo 4 <c c'>32 \grace {dis'8} \repeat tremolo 8 <e, e'>64 ~
        c \acciaccatura {gis8} a16 f d g8 \repeat tremolo 4 <c, c'>32 ~ \repeat tremolo 8 <c c'>32 ~ \repeat tremolo 4 <c c'>32 <dis dis'>16 \repeat tremolo 4 <e e'>64 ~
        \tempo 4 = 88
        \repeat tremolo 48 <e e'>64 ~ <e e'>16 <f f'>8 \acciaccatura {cis'8} d16 ~
        d c32 bes a16 g \tuplet 3/2 { f32 g e } d16 \tuplet 3/2 { c32 d c} bes16 \tuplet 3/2 { a32 ais b-"* (Bb)" } g16 \tuplet 3/2 {f32 g f} d16 c bes a g
        bes d f a c \parenthesize d, \tuplet 3/2 { f a c } aes bes c f ees aes, aes' bes
        \acciaccatura {gis8} a8 \tuplet 3/2 { f16 d bes } a16 \parenthesize g a'8 \tuplet 3/2 { f16 d bes } a16 \parenthesize g a'8 \tuplet 3/2 { f16 d bes }
        \tempo 4 = 89
        a16 \parenthesize g a'8 \tuplet 3/2 { f16 d bes } b16-"* (A)" \parenthesize g c'16 \acciaccatura {bes8} aes16 a16 f ees des c bes
        \acciaccatura {gis8} a16 d, f a bes g e g c a32 f d16 d' bes g e8
        e'16 c32 bes a16 f bes d f g aes des, e <ees ees'> ~ q des'8 \acciaccatura {b8} c16 ~
        c8 bes16 aes \appoggiatura {fis8} g ees16 c f8 <b c>4 r8
        \tuplet 6/4 { r8 f16 aes bes c } \tuplet 6/4 { bes aes f aes bes c } \tuplet 6/4 { bes aes d,-"* (E)" aes' bes c } \tuplet 6/4 { bes aes d,-"* (E)" aes' bes c }
        \tuplet 6/4 { aes-"* (Bb)" aes f aes bes c } \tuplet 6/4 { bes aes f aes bes c } \tuplet 6/4 { bes aes f aes bes c } \tuplet 6/4 { bes aes f aes bes c }
        \tuplet 6/4 { bes aes f aes bes c } \tuplet 6/4 { bes aes e aes bes c } \tuplet 3/2 { bes8 aes16 } bes8 r8. ees32 des
        c16 g bes aes \tuplet 3/2 { g32 aes g } f16 \tuplet 3/2 { ees32 f ees } c16 \tuplet 3/2 { bes32 c bes } aes16 \tuplet 3/2 { g32 aes g } f16 \tuplet 3/2 { ees32 f ees } c16 bes aes
        g32 \parenthesize aes c ees \parenthesize aes, c \parenthesize e f c32 \parenthesize d g bes d, \parenthesize fis aes32 c des, \parenthesize  f bes des aes16 d32 dis e f fis g gis a ais b
        \tempo 4 = 90
        c8. bes32 aes g16 f ees c bes aes \acciaccatura {fis8} g16 ees f aes c ees
        aes, c ees g c, ees f <bes, bes'> <b b'> \repeat tremolo 6 <c c'>32 ~ \repeat tremolo 4 <c c'>32 \tuplet 3/2 { f16 a c }
        \tempo 4 = 89
        \acciaccatura {dis8} e8 c16 g d'8 <f, f'>8 ~ q4 e'32 f e c e f e c
        e f e c e f e c e f e c e8 e32 f e ees d des c b bes a aes ges f ges f ees
        \tempo 4 = 90
        d32 c bes \parenthesize aes g32 gis a fis g a bes g a bes c a bes c d bes c d e c d e f d e f g d_"* (E)"
        f g a f \parenthesize g a bes g c g bes c d8 r16 c32 bes aes16 aes_"* (G)" f ees d c
        \tempo 4 = 89
        \acciaccatura {bes8} a16 f d g, <g g'> <c c'> <f f'> <a a'> <c c'>8 <e e'>8 r4
        r16 <ges,, ges'> <aes aes'> <c c'> <ees ees'> <f f'> <ges ges'> <aes aes'> <bes bes'> <c c'> <cis cis'> <d d'> <dis dis'> <e e'> <f f'> \acciaccatura {cis'8} d16 ~
        \tempo 4 = 91
        d16 c8 bes16 \acciaccatura {gis8} a8 f16 d g <c,c'>^> a' f g <c, c'>^> a' f g <c, c'>8^> <c  c'>16^> ~ q a' f g c <des, des'>^> aes' e aes8 des16 \acciaccatura {b8} c16 ~
        \tempo 4 = 89
        c bes32 aes g16 f ees g c, aes  bes8 c16 \acciaccatura {fis,8} g16 ~ g ees8 <f, f'>16 ~
        q f8. f'32 f f f f f f f f f aes bes d_"* (C)" bes aes f aes bes c bes aes f aes bes
        \tempo 4 = 91
        c bes aes f aes bes c bes aes f aes bes c bes aes f aes bes c bes aes f aes bes c bes aes f aes bes c bes
        aes f aes bes c bes aes fis fis gis a b cis g a \parenthesize b d a b cis dis d des c ees e fis g gis32 a ais b
        \tempo 4 = 94
        c8.. bes32 aes g16. f16 ees c bes aes \acciaccatura {fis8} g16 \tuplet 3/2 { ees f8 } c'
        \tempo 4 = 88
        \tuplet 6/4 { r8 c,16( f a c) } f,16 g c ees bes des e aes <ees ees'>8 <ees des'>16 ees'
        \tempo 4 = 91
        \acciaccatura {b8} c8 bes16 aes g f c'8^> bes16 aes g f c'16^> bes32 aes g16 f
        c'8^> bes16 aes g f c'^> bes32 aes ges16 ees c'8^> bes c16 \acciaccatura {gis8} a16
        \tempo 4 = 90
        r16 <f f'>8.^> q16^> d' \acciaccatura {aes8} g16 d16 f8 f16 \parenthesize f <aes, aes'> <a a'> <bes bes'> <b b'>
        <c c'> \acciaccatura {gis'8} a8. r8. <f, f'>16 <a a'> <bes bes'> <b b'> <c c'> r4
        r16 <f, f'> <a a'> <bes bes'> <b b'> <c c'> \appoggiatura {<aes aes'>8} <a a'>8 <f f'> <d d'> <f f'>8 r16 f'32^> f
        \tempo 4 = 91
        f f f f f^> f f f f f f^> f f f f f f^> f f f fis g gis a bes b c cis d16 f16 ~
        f16 \acciaccatura {cis8} <cis d>8. c16 bes \tuplet 3/2 {a32 bes a} g16 \tuplet 3/2 {f32 g f} d16 <gis, a> bes c bes a bes
        \tempo 4 = 90
        d <f, f'> <a a'> <c c'> <f, f'> r8. r16 \acciaccatura {cis''8} d8 c16 ~ c <g aes> f d
        f8 \acciaccatura {cis8} <b cis> \acciaccatura {aes8} <g aes>16 f d c g' f d c d8 c
        r16 bes c d ees e gis c ees e gis ees' ~ ees8. r16
        \tempo 4 = 89
        r16 f,,,8 bes16 ~ bes ees8 bes'16 ~ bes ees8 g16 ~ g d'8. ~
        d4 ~ <<
          { \voiceOne
            d8. ees,16 ~ ees bes8 aes16 ~ aes ees8 bes'16
          }
          \new Voice {
            \voiceTwo
            s8. e,16 ~ e2
          }
        >> \oneVoice
        \acciaccatura {fis8} g8 ees f <aes, c> ~ q2
        \tuplet 3/2 { r8 g aes } \tuplet 3/2 { c ees f } g e16 bes16 ~ bes c'8 bes16
        \appoggiatura {aes8} g8. f16 ~ f2 ~ f16 c'32 bes aes8
        g f16 g c,8 c16 des ~ des ees'8. ~ ees16 des8 c16 ~
        c16 ees,8 g'16 ~ g bes8. ~ bes2
        r4 r8. <c,, fis>16 ~ q4 ~ q8. <d f>16 ~
        q4
        

      }

    }

  \layout {}

}
