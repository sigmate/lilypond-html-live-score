# LilyPond HTML Live Score

## Description

This tool aims to provide an attractive way to visualize music scores produced
using LilyPond.

It relies on LilyPond's SVG backend output to create an HTML file with
additionnal Javascript code which one can play in any decent web browser
(it appeared that Firefox might be the slowest for this task, Chrome performs
a lot faster).

The music score is animated in realtime while an audio file is played along.

## Development stage

This project is at an early stage of development. There's a lot of things to
improve, ideas to explore. Any contributions welcome!

## Process overview

A single Python script will handle successive tasks in order to produce an HTML
output file.

* It runs LilyPond on your .ly file to produce an SVG file.

* When running LilyPond it includes some code ("grob-inspector.ily") via a
command line argument that adds extra timing and musical information to the
SVG output (using LilyPond's 'output-attributes' grob property).

* Computations are performed (in "grob-inspector.ily") to determine the exact
time position of each grob in seconds (depending on tempo changes, of course).

* Finally, the python script gathers everything into an HTML file (embedding the
SVG into the HTML).

## Example

The repository contains scores and audio files for demonstration purpose.

To run the tool and watch the demos, issue the following command from the base
directory (not from the examples directory:

    ./make-live-score -i examples/pools.ly -o pools.html \
    --ogg-vorbis-file=examples/pools.ogg --m4a-file=examples/pools.m4a

    ./make-live-score -i examples/tetris.ly -o tetris.html \
    --ogg-vorbis-file=examples/tetris.ogg

    ./make-live-score -i examples/messiaen.ly -o messiean.html \
    -ogg-vorbis-file=examples/messiaen.ogg

## Requirements

Requires LilyPond 2.19.49 or higher (for 'output-attributes' grob property).

## Credits

Mathieu Demange & Paul Morris

## License

This tool is release under the terms of the GNU GPL 3 license (see LICENSE.md).
