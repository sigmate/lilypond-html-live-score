
var intervalCounterValue = 100;
var timeOffset = 0.1;
var audioElement;
var currentGlyph = 0;
var glyphs = [];
var intervalID;
var audioTime;

function makeSomeGlyphsBlack() {
    while (glyphs[currentGlyph].time < audioTime) {
        glyphs[currentGlyph].node.setAttribute("fill-opacity", "1.0");
        glyphs[currentGlyph].node.setAttribute("stroke-opacity", "1.0");
        currentGlyph++;
    }
}

function svgCallback() {
    audioTime = audioElement.currentTime + timeOffset;
    if (currentGlyph < glyphs.length) {
        window.requestAnimationFrame(makeSomeGlyphsBlack);
    } else {
        window.clearInterval(intervalID);
    }
}

function makeGlyphsGrey() {
    glyphs.forEach(function (glyph) {
        glyph.node.setAttribute("fill-opacity", "0.5");
        glyph.node.setAttribute("stroke-opacity", "0.5");
    });
}

function playHandler(event) {
    makeGlyphsGrey();
    currentGlyph = 0;
    intervalID = window.setInterval(svgCallback, intervalCounterValue);
}

window.addEventListener("load", function() {

    var gNodes = document.querySelectorAll("g.ly.grob");

    var grobClickHandler = function(event) {
        makeGlyphsGrey();
        currentGlyph = 0;
        audioElement.currentTime = parseFloat(this.getAttribute("data-time"));
        audioElement.play();
    };

    // Populate the glyphs array.  It stores pointers to the
    // child nodes (glyphs) of <g> nodes (grobs) with timing data
    // for each glyph.  This gives us a flat array rather than an
    // array of grobs with a nested array of glyphs for each grob.
    for (var i = 0; i < gNodes.length; i += 1) {

        var gNode = gNodes[i];
        gNode.addEventListener("click", grobClickHandler, false);

        var gNodeTime = parseFloat(gNode.getAttribute("data-time"));
        var gNodeChildren = gNode.children;

        for (var c = 0; c < gNodeChildren.length; c += 1) {
            glyphs.push({
                time: gNodeTime,
                node: gNodeChildren[c]
            });
        }
    }

    glyphs.sort(function(a, b) { return a.time - b.time; });

    makeGlyphsGrey();

    audioElement = document.getElementById("audioElement");
    audioElement.addEventListener("play", playHandler, false);
});
