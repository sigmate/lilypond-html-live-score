<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>${audioTitle} - lilypond-html-live-score</title>
        <link type="text/css" href="css/lilypond-html-live-score.css" rel="stylesheet" />
        <script type="text/javascript" src="js/lilypond-html-live-score.js"></script>
    </head>

    <body>

    <div id="audioContainer">
        <audio id="audioElement" controls="controls">
            Your browser does not support the <code>audio</code> element.
            <source src="${audioM4a}" type="audio/mp4"></source>
            <source src="${audioOgg}" type="audio/ogg"></source>
            <source src="${audioMp3}" type="audio/mpeg"></source>
        </audio>
    </div>

    <div id="svgContainer">
        ${svg}
    </div>

    <div id="debug"></div>

    </body>

</html>
